<?php

#app\Http\Controllers\HomeController.php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the USER dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the ADMIN dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        return view('adminHome');
    }


    // Menampilkan form password
    public function showChangePasswordGet() {
        return view('auth.passwords.change-password');
    }

    // Mengirim password baru & Validasi
    public function changePasswordPost(Request $request) {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            // Current password and new password same
            return redirect()->back()->with("error","New Password cannot be same as your current password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:8|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password successfully changed!");
    }

    // Fungsi ganti status dan upload Bukti
    public function showStatusAkunGet() {
        return view('statusAkun');
    }

    public function statusAkunPost(Request $request) {
        $user = Auth::user();
        if ($image = $request->file('upload_bukti')) {
            // Hapus file lama
            if ($user->bukti != NULL) {
                File::delete(public_path("bukti/$user->bukti"));
            }
            $destinationPath = 'bukti/';
            $profileImage = $user->name . '_bukti' . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $user->bukti = "$profileImage";
            $user->status = 'positif';
            $user->update();
            return redirect()->back()->with("success","Bukti status covid berhasil diupload!");
        }else{
            unset($input['image']);
        }
    }

    public function deleteStatusPost(Request $request) {
        $user = Auth::user();
        // Hapus file lama
        if ($user->bukti != NULL) {
            File::delete(public_path("bukti/$user->bukti"));
        }
        $user->status = 'negatif';
        $user->update();
        return redirect()->back()->with("success","Selamat telah sembuh dari COVID-19!");
    }
}
