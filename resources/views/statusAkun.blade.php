@extends('adminlte::page')

@section('title', 'Bukti COVID')

@section('content_header')
    <h1>Bukti Terkena COVID</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 ">
            <div class="panel panel-default">

                {{-- Alert --}}
                <div class="panel-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if($errors)
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                        @endforeach
                    @endif

                    {{-- Status Pengguna Akun --}}
                    <div class="status_akun col-md-10">
                        <h5>
                            Status COVID-19 anda adalah <b> {{ Auth::user()->status }} </b>
                        </h5>
                        @if (Auth::user()->status=='positif')
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalStatus">
                                    Bukti Positif
                                </button>
                                {{-- Masih belum bisa --}}
                                {{-- <form action="{{ route('deleteStatusPost') }}" method="post"> {{ csrf_field() }}
                                    <button type="button" class="btn btn-success">
                                        Sudah Sembuh
                                    </button>
                                </form> --}}
                            </div>
                        @endif
                    </div>

                        {{-- Form Isian --}}
                            <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{ route('statusAkunPost') }}">
                                {{ csrf_field() }}

                                <div class="form-group{">
                                    <label for="upload_bukti" class="col-md-10 control-label">Upload tanda positif COVID-19 berupa file gambar</label>

                                    <div class="col-md-10">
                                        <input id="upload_bukti" type="file" class="form-control" name="upload_bukti" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4 mt-2">
                                        <button type="submit" class="btn btn-primary">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


  <!-- Modal -->
  <div class="modal fade" id="modalStatus" tabindex="-1" aria-labelledby="labelModal" aria-hidden="true">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelModal">Bukti Positif COVID-19</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-auto">
          <img src="{{asset('/bukti/'.Auth::user()->bukti)}}" alt="Bukti Postifi COVID" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://kit.fontawesome.com/f68e3b150b.js" crossorigin="anonymous"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script>

    </script>
@stop
