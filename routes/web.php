<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/login', function () {
    return view('login');
});
//Akses logout lewat url
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//verifikasi email user
Auth::routes(['verify' => true]);

// Route Admin Home & Cek Admin atau bukan
Route::group(['middleware' => 'is_admin'], function () {
    Route::get('admin/home', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home');

    Route::get('admin', function () {
        return redirect()->route('admin.home');
    });
});

// Route::get('admin/home', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
// Route::get('admin', function () {
//     return redirect()->route('admin.home');
// })->middleware('is_admin');

// Untuk menampilkan form password dan ganti password
Route::get('/changePassword', [App\Http\Controllers\HomeController::class, 'showChangePasswordGet'])->name('changePasswordGet');
Route::post('/changePassword', [App\Http\Controllers\HomeController::class, 'changePasswordPost'])->name('changePasswordPost');

// Menampilkan form upload bukti dan update status
Route::get('/statusAkun', [App\Http\Controllers\HomeController::class, 'showStatusAkunGet'])->name('statusAkunGet');
Route::post('/statusAkun', [App\Http\Controllers\HomeController::class, 'statusAkunPost'])->name('statusAkunPost');
// Route::post('/deleteStatus', [App\Http\Controllers\HomeController::class, 'deleteStatusPost'])->name('deleteStatusPost');
